#!/bin/bash

bold=$(tput bold)
underscore=$(tput smul)
normal=$(tput sgr0)

AWS_DEFAULT_PROFILE=oohlala
#AWS_DEFAULT_PROFILE=dublabs
REGION=us-east-1
#FILE=dublabs-us-east-1

function detail_zone {
  zone=$1

  printf "\tChecking details for: $zone\n"
#  records=(`aws --no-cli-pager --no-paginate route53 list-resource-record-sets --hosted-zone-id $zone --output json --query 'ResourceRecordSets[*]' | jq '[.[] | {name: .Name, type: .Type, records: (.ResourceRecords // [] | map(.Value) | join(",") | if .=="" then "null" else . end)}]' | jq -jr '[.[] | .name, .type, .records]'  | tr -d '[],"'`)
  aws --no-cli-pager --no-paginate route53 list-resource-record-sets --hosted-zone-id $zone --output json --query 'ResourceRecordSets[*]' | jq '[.[] | {name: .Name, type: .Type, records: (.ResourceRecords // [] | map(.Value) | join(",") | if .=="" then "null" else . end)}]' | jq -jr '[.[] | .name, .type, .records]'
#aws --no-cli-pager --no-paginate route53 list-resource-record-sets --hosted-zone-id $zone --output json --query 'ResourceRecordSets[*]' | jq '[.[] | {name: .Name, type: .Type, records: (.ResourceRecords // [] | map(.Value) | join(",") | if .=="" then "null" else . end)}]' | jq -r '.[] | .name,.type, .records'
#  for record in "${records[@]}"; do
#    echo $record
#  done
#    echo "----"
#    exit
#    name=`echo "$record" | jq -r ".Name"`
#    type=`echo "$record" | jq -r ".Type"`
#    resource=`echo "$record" | jq -r ".ResourceRecords[]"`
#    printf "\t$name - $type - $resource\n"
#  done
}

zones=(`aws --no-cli-pager --no-paginate route53 list-hosted-zones --output json --query 'HostedZones[*].Id' | tr -d '[],"'`)
for zone in "${zones[@]}"; do
    hosted=`aws --no-cli-pager --no-paginate route53 get-hosted-zone --id $zone`
    name=`echo $hosted | jq -r '.HostedZone.Name'`
    comment=`echo $hosted | jq -r '.HostedZone.Config.Comment'`
    private=`echo $hosted | jq -r '.HostedZone.Config.PrivateZone'`
    label=""
    if [[ $private == "true" ]]; then
      label="private"
    else
      label="public"
    fi

#    echo "${bold}$name${normal} - $comment | $label"
    detail_zone $zone
    exit
done
