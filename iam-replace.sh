#!/bin/bash

bold=$(tput bold)
underscore=$(tput smul)
normal=$(tput sgr0)

BASTION_USER=robin_de_jong
BASTION_KEY=~/.ssh/id_ed25519

LC_ALL=C
region=ca-central-1
SECURITY_GROUP=prod-can-web-api

USER=centos
instance_ids=($(./scripts/aws-asg-info $SECURITY_GROUP $region | tr -d \'\"))

IAM_ROLE=$1

function apply_iam() {
  instance=$1
  iam=$2
  echo "${bold}Applying on $iam onto $instance${normal}"
  association=$(aws ec2 describe-iam-instance-profile-associations --filters "Name=instance-id,Values=$instance" --region=$region | jq -r ".IamInstanceProfileAssociations[0].AssociationId")
  echo "${bold}Replacing association: $association${normal}"
  aws ec2 replace-iam-instance-profile-association --iam-instance-profile Arn=$iam --association-id $association  --region=$region
}

info $template_server
for i in "${instance_ids[@]}"; do
  apply_iam $i $IAM_ROLE
done
