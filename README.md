# SSH stuff


## Dependencies

- AWS CLI
- jq
- gnu-getopt (for OSX install it with: `brew install gnu-getopt && brew link --force gnu-getopt`, the default getopt won't work)
- dos2unix
- ksh93

## Notes

```bash
./scripts/aws-ssh --bu robin_de_jong --bk ~/.ssh/id_ed25519 i-0f577287438eb21d1
```


JQ to Array:

```bash
array=(`aws command | jq -r ... | tr -d '[],"'`)
array=(`aws command | jq -r '... | @sh' | tr -d \'\"`) 
```

## Inventorize

Fetching all the resources in the security groups:
```bash
# Subnet subnet-prod-public-a - subnet-08a7ef77b2d3c09a9
aws ec2 describe-network-interfaces --filters Name=subnet-id,Values=subnet-08a7ef77b2d3c09a9 --region "us-east-1" --output json --query 'NetworkInterfaces[*]'.['Description','PrivateIpAddress','VpcId']
# Subnet subnet-prod-public-b - subnet-0b92caf1b442c8756
aws ec2 describe-network-interfaces --filters Name=subnet-id,Values=subnet-0b92caf1b442c8756 --region "us-east-1" --output json --query 'NetworkInterfaces[*]'.['Description','PrivateIpAddress','VpcId']
# Subnet subnet-prod-public-c - subnet-0ae422624425d9360
aws ec2 describe-network-interfaces --filters Name=subnet-id,Values=subnet-0ae422624425d9360 --region "us-east-1" --output json --query 'NetworkInterfaces[*]'.['Description','PrivateIpAddress','VpcId']

# Subnet subnet-prod-private-a - subnet-074366096d343654c
aws ec2 describe-network-interfaces --filters Name=subnet-id,Values=subnet-074366096d343654c --region "us-east-1" --output json --query 'NetworkInterfaces[*]'.['Description','PrivateIpAddress','VpcId']
# Subnet subnet-prod-private-b - subnet-0876b2b5e631f0026
aws ec2 describe-network-interfaces --filters Name=subnet-id,Values=subnet-0876b2b5e631f0026 --region "us-east-1" --output json --query 'NetworkInterfaces[*]'.['Description','PrivateIpAddress','VpcId']
# Subnet subnet-prod-private-c - subnet-02d3491db2abb3578
aws ec2 describe-network-interfaces --filters Name=subnet-id,Values=subnet-02d3491db2abb3578 --region "us-east-1" --output json --query 'NetworkInterfaces[*]'.['Description','PrivateIpAddress','VpcId']
```

## Crontab
Crontab is interesting,
Use `watch.sh` to quickly check which stuff you need to wait for:

```bash
#!/bin/bash
while true
do
    echo $(ps aux | grep "python") | tee -a test.log
    sleep 1
done
```



Maintenance (stag):
```
oohlala 14387 0.0 0.0 106076 1308 ? Ss 16:14 0:00 /bin/sh -c logger $(/home/oohlala/oohlala_env_py3/bin/python /home/oohlala/oohlala-offline-scripts/oohlala_offline_scripts/run.py) 
oohlala 14388 0.0 0.0 106076 656 ? S 16:14 0:00 /bin/sh -c logger $(/home/oohlala/oohlala_env_py3/bin/python /home/oohlala/oohlala-offline-scripts/oohlala_offline_scripts/run.py) 
oohlala 14389 39.0 1.2 279300 49012 ? S 16:14 0:01 /home/oohlala/oohlala_env_py3/bin/python /home/oohlala/oohlala-offline-scripts/oohlala_offline_scripts/run.py
 
root 14419 0.0 0.0 103296 836 pts/0 S+ 16:14 0:00 grep python 
root 28431 0.0 4.4 356444 172828 ? SN Nov16 3:58 /usr/bin/python /usr/bin/denyhosts.py --daemon --config=/etc/denyhosts.conf
```


Offline (prod): 
```

```











sudo su -
less /home/oohlala/oohlalamobile/settings.py