#!/bin/bash
AWS_DEFAULT_PROFILE=dublabs
#AWS_DEFAULT_PROFILE=oohlala
# Platform
#REGION="us-east-1"
#REGION="ca-central-1"
REGION="us-west-2"


dbs=(`aws --no-cli-pager --no-paginate rds describe-db-instances --region $REGION --filter 'Name=instance-state,Values=running' | jq --compact-output '.DBInstances[] | {"name": .DBInstanceIdentifier, "type": .DBInstanceClass}'`)
#echo ${dbs[@]}
for db in "${dbs[@]}"; do
    name=`echo $db | jq -r '.name'`
    type=`echo $db | jq -r '.type'`
    echo -e $name "\t" $type
done
