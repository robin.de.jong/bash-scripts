#!/bin/bash

bold=$(tput bold)
underscore=$(tput smul)
normal=$(tput sgr0)

#AWS_DEFAULT_PROFILE=oohlala
AWS_DEFAULT_PROFILE=dublabs
FILE=dublabs-us-east-1

HEADER_LINE="Active Internet connections (w/o servers)\nProto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name"

standard_services=("auditd.service" "chronyd.service" "cloud-config.service" "cloud-final.service" "cloud-init-local.service"
"cloud-init.service" "crond.service" "dbus.service" "getty@tty1.service" "gssproxy.service" "kdump.service" "kmod-static-nodes.service"
"network.service" "polkit.service" "postfix.service" "rhel-dmesg.service" "rhel-domainname.service" "rhel-import-state.service"
"rhel-readonly.service" "rsyslog.service" "serial-getty@ttyS0.service" "sshd.service" "systemd-journal-flush.service" "systemd-journald.service"
"systemd-logind.service" "systemd-random-seed.service" "systemd-remount-fs.service" "systemd-sysctl.service" "systemd-tmpfiles-setup-dev.service"
"systemd-tmpfiles-setup.service" "systemd-udev-trigger.service" "systemd-udevd.service" "systemd-update-utmp.service" "systemd-user-sessions.service"
"systemd-vconsole-setup.service" "tuned.service" "atd.service" "irqbalance.service" "rhel-autorelabel-mark.service" "amazon-cloudwatch-agent" "rpcbind.service"
"selinux-policy-migrate-local-changes@targeted.service" "ntpd.service" "rpcbind.service" "sshd-keygen.service" "systemd-hwdb-update.service"
"systemd-journal-catalog-update.service" "systemd-update-done.service" "amazon-efs-mount-watchdog.service" "jexec.service")

function unquote {
  echo $@ | awk '{print substr($0, 2, length($0) - 2)}'
}

function sanitize {
  echo -n $1 | perl -pe 's/[\?\[\]\/\\=<>:;,''"&\$#*()|~`!{}%+]//g;' -pe 's/[\r\n\t -]+/-/g;'
}

function filterServices {
  services=$1
  out=""
  for service in ${standard_services[@]}; do
    out+="|$service"
  done
  out="(${out:1})"
  printf "$services\n" | grep -vwE "$out"
}

function exec {
  user=$1
  instance=$2
  public=$3
  region=$4

  if [[ $public == "true" ]]; then
    aws-ssh --public -r $region -u $user $instance -n "$5"
  else
    aws-ssh -r $region  -u $user $instance -n "$5"
  fi
}

function info {
  name=$1
  user=$2
  instance=$3
  public=$4
  region=$5
  echo "Checking ${bold}$name${normal}, with user: ${bold}$user${normal} ($instance, $asg})"

  sanitized=`sanitize "$instance-$name"`
  path="files/$FILE/$sanitized"
  mkdir -p $path
  connections=`exec $user $instance $public $region "sudo netstat -tupn | grep -E \".*ESTABLISHED\""`
  printf "$HEADER_LINE\n" > "$path/connections.txt"
  printf "$connections\n" >> "$path/connections.txt"
  services=`exec $user $instance $public $region "systemctl list-units --type=service --state=active"`
  filtered=`filterServices "$services"`
  if [[ ! -z $filtered ]]; then
    printf "$filtered\n" >> "$path/services.txt"
  fi

  cron=`exec $user $instance $public $region "sudo cat /var/log/cron | grep -vwE 'cron.hourly|cron.daily|anacron'"`
  if [[ ! -z $cron ]]; then
    printf "$cron\n" >> "$path/cron.txt"
  fi

  last=`exec $user $instance $public $region "sudo last -F | grep -vwE '172-17-2-123|127.17.2.123'"`
  if [[ ! -z $last ]]; then
    printf "$last\n" >> "$path/last.txt"
  fi
}

rm -rf files/$FILE

while IFS=, read -r name instance type status asg user public region
do
  # First line will be the headers, don't include them
  # Also skip entries that start with a `#`.
  if [[ $name == "name" ]] || [[ "${name:0:1}" == "#" ]] || [[ "`unquote $asg`" == "true" ]]; then
    continue
  fi

  user=`unquote $user`
  instance=`unquote $instance`
  public=`unquote $public`
  name=`unquote $name`
  region=`unquote $region`

  info "$name" "$user" "$instance" "$public" "$region"
done < csv/$FILE.csv
