#!/bin/bash
AWS_DEFAULT_PROFILE=dublabs
#AWS_DEFAULT_PROFILE=oohlala
# Platform
#REGION="us-east-1"
#REGION="ca-central-1"
REGION="us-west-2"
#SUBNET_GROUPS=(
#"subnet-08a7ef77b2d3c09a9" # subnet-prod-public-a
#"subnet-0b92caf1b442c8756" # subnet-prod-public-b
#"subnet-0ae422624425d9360" # subnet-prod-public-c
#"subnet-074366096d343654c" # subnet-prod-private-a
#"subnet-0876b2b5e631f0026" # subnet-prod-private-b
#"subnet-02d3491db2abb3578" # subnet-prod-private-c
#"subnet-d754719f" # us-production-public
#)


#REGION="ca-central-1"
#SUBNET_GROUPS=(
##"subnet-06991b372128050cb" # subnet-prod-can-private-a
##"subnet-00fd344af9b2f22a9" # subnet-prod-can-private-b
##"subnet-0ff03cff12de577d0" # subnet-prod-can-private-d
##"subnet-07771db27c10952c9" # subnet-prod-can-public-a
##"subnet-01c54d943e3ff556b" # subnet-prod-can-public-b
##"subnet-05159cf2c8f1f4a5a" # subnet-prod-can-public-d
##"subnet-0105d1e9739/57a5be" # ca-production-lambda-1a
##"subnet-0f81264b127e68fef" # ca-production-lambda-1b
##"subnet-551ad33c" # Public subnet
##"subnet-8de8aff6" # Public subnet 1b
##"subnet-3b7a5452" # Public subnet 2
#"subnet-06991b372128050cb" # subnet-prod-can-private-a
#"subnet-00fd344af9b2f22a9" # subnet-prod-can-private-b
#"subnet-0ff03cff12de577d0" # subnet-prod-can-private-d
#)

# Dublabs
#REGION="us-west-2"
#SUBNET_GROUPS=(
#"subnet-05d5e98f936f2477a" # subnet-prod-operations-private-a
#"subnet-0c83d1528a364aa9a" # subnet-prod-operations-private-b
#"subnet-0a2a558aabc416814" # subnet-prod-operations-private-c
#"subnet-0b0bdf506dbbd3e61" # subnet-prod-operations-public-a
#"subnet-02f5d1987c28f517e" # subnet-prod-operations-public-b
#"subnet-00749cd9e37e81c03" # subnet-prod-operations-public-c
#)
#
#
#REGION="us-east-1"
#SUBNET_GROUPS=(
#"subnet-0f59c9847752246e4" # subnet-private-prod-us-east-1a
#"subnet-068eb8849dfd48d0d" # subnet-private-prod-us-east-1c
#"subnet-06bcd8c69825ef0ce" # subnet-public-prod-us-east-1a
#"subnet-0e713fbdb8b1c172d" # subnet-public-prod-us-east-1b
#"subnet-0910f1b1709ab16fb" # subnet-prod-operations-private-a
#"subnet-063b7cc6154ee9058" # subnet-prod-operations-private-b
#"subnet-0902dc33af339630c" # subnet-prod-operations-private-c
#"subnet-003ecfb9499a5e1a4" # subnet-prod-operations-private-d
#"subnet-028d02254d4cc22fa" # subnet-prod-operations-public-a
#"subnet-0874174409ee32c70" # subnet-prod-operations-public-b
#"subnet-075e5b6e6379726b5" # subnet-prod-operations-public-c
#"subnet-01fdf6bc09be81e8c" # subnet-prod-operations-public-d
#)

instance_ids=(`aws --no-cli-pager --no-paginate ec2 describe-network-interfaces --region $REGION --filters Name=status,Values=in-use --output json --query 'NetworkInterfaces[*].Attachment.InstanceId' | tr -d '[],"'`)
#echo ${instance_ids[@]}
#
echo "name,type,subnet,asg" > test.csv

for i in "${instance_ids[@]}"; do
  details=`aws --no-cli-pager ec2 describe-instances --instance-ids $i --region $REGION --output json`
  status=`echo $details | jq -r '.Reservations[0].Instances[0].State.Name'`
  if [ $status != "running" ]; then
    continue
  fi

  asg=`echo $details | jq '.Reservations[0].Instances[0].Tags | any(.[]; .Key == "Provision")'`
  name=`echo $details | jq -r '.Reservations[0].Instances[0].Tags | .[] | select(.Key=="Name") | .Value'`
  type=`echo $details | jq -r '.Reservations[0].Instances[0].InstanceType'`
  subnet=`echo $details | jq -r '.Reservations[0].Instances[0].NetworkInterfaces[0].SubnetId'`
#  echo $status
#  echo $details | jq
#  read -p "Press [Enter] key to start backup..."

  name=`echo $details | jq -r '.Reservations[0].Instances[0].Tags | .[] | select(.Key=="Name") | .Value'`
  ip=`echo $details | jq -r '.Reservations[0].Instances[0].NetworkInterfaces[0].PrivateIpAddress'`
  echo "${bold}$name${normal} ($i)"
  echo "$name,$type,$subnet,$asg" >> test.csv
done