#!/bin/bash

bold=$(tput bold)
underscore=$(tput smul)
normal=$(tput sgr0)

BASTION_USER=robin_de_jong
BASTION_KEY=~/.ssh/id_ed25519

LC_ALL=C

COMMAND="uname -a"

while IFS=, read -r name instance ip user region
do
  # First line will be the headers, don't include them
  # Also skip entries that start with a `#`.
  if [[ $name == "name" ]] || [[ "${name:0:1}" == "#" ]]; then
    continue
  fi

  echo "Checking ${bold}$name${normal}, with user: ${bold}$user${normal} ($instance, $ip)"
  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -r $region -u $user -n $instance "$COMMAND"
  echo
done < platform/ca-central-1/servers.csv

sg=prod-can-web-api
region=ca-central-1
user=centos
instance_ids=(`./aws-asg-info $sg $region | tr -d \'\"`)
for i in "${instance_ids[@]}"; do
  echo "Instance: $i"
  echo "Checking from asg: ${bold}$sg${normal} instance: ${bold}$i${normal}"
  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $user -n $i "$COMMAND"
  echo
done