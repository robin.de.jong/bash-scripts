#!/bin/bash

bold=$(tput bold)
underscore=$(tput smul)
normal=$(tput sgr0)

BASTION_USER=robin_de_jong
BASTION_KEY=~/.ssh/id_ed25519

LC_ALL=C
SECURITY_GROUP=prod-can-web-api
#SECURITY_GROUP=prod-web-api
#region=us-east-1
region=ca-central-1
USER=centos
instance_ids=(`./scripts/aws-asg-info $SECURITY_GROUP $region | tr -d \'\"`)
template_server="i-04c93419b5fb14185" # Can
#template_server="i-0b738bc92a5d05614"
LOG_CONFIG=$(cat << EOF
/var/log/oohlala/*.log {\n
\thourly\n
\trotate 3\n
\tsize 10M\n
\tcompress\n
\tdelaycompress\n
\tpostrotate\n
\t\tuwsgi --reload /var/run/uwsgi.pid\n
}
EOF)
LOG_UPDATE_COMMAND=$(cat << EOF
sudo -u root -- sh -c "
echo -e \"$LOG_CONFIG\" > /etc/logrotate.d/core-log-rotate.conf
"
EOF)


function update_log_rotation() {
  instance=$1
  echo "Going to update the log on: ${bold}$instance${normal}"

  command="sudo ln -s /etc/cron.daily/logrotate /etc/cron.hourly/logrotate"
  restart="sudo systemctl restart crond"
  remove="sudo rm /var/log/oohlala/api_request.log-*"

    aws-ssh -u $USER $instance "$LOG_UPDATE_COMMAND" -r $region
    aws-ssh -u $USER $instance "sudo uwsgi --reload /var/run/uwsgi.pid" -r $region
}

update_log_rotation "$template_server"
for i in "${instance_ids[@]}"; do
  update_log_rotation $i
done
#update_log_rotation "i-075c18a8061fb1490"
