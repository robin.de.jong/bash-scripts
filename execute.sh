#!/bin/bash

bold=$(tput bold)
underscore=$(tput smul)
normal=$(tput sgr0)

LC_ALL=C

action=$1
module=$2
commands=("start" "stop" "dry-run")
if [[ ! " ${commands[*]} " =~ " ${action} " ]]; then
  echo "Not a valid command $action"
  exit
fi

BASTION_USER=robin_de_jong
BASTION_KEY=~/.ssh/id_ed25519

load() {
  module=$1
  action=$2
  read -p "Press [Enter] key to use module: ${bold}$module${normal} and execute action: ${bold}$action${normal}"
  source $1 $2
}

modules=(
  "core-api.sh"
  "core-workers.h"
  "offline-maintenance.sh"
  "chat.sh"
  "odin.sh"
  "odin-workers.sh"
  "airflow.sh"
  "es-indexer.sh"
  "site-main.sh"
  "mode-bridge.sh"
)

regions=(
  "us"
  "ca"
)

load_module=$(echo $module | sed 's/.*\///' | cut -d. -f1)

for region in ${regions[@]}; do
  if [ -z "$module" ]; then
    for module in ${modules[@]}; do
      load "module/$region/$module" $action
    done
  else
    load "module/$region/$load_module.sh" $action
  fi
done
