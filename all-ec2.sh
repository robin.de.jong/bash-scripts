#!/bin/bash
AWS_DEFAULT_PROFILE=dublabs
#AWS_DEFAULT_PROFILE=oohlala
# Platform
#REGION="us-east-1"
#REGION="ca-central-1"
REGION="us-west-2"

instance_ids=(`aws --no-cli-pager --no-paginate ec2 describe-network-interfaces --region $REGION --output json --query 'NetworkInterfaces[*].Attachment.InstanceId' | tr -d '[],"'`)

csv_name=csv/$AWS_DEFAULT_PROFILE-$REGION.csv
echo "name,instance,type,status,asg" > $csv_name
for i in "${instance_ids[@]}"; do
  details=`aws --no-cli-pager ec2 describe-instances --instance-ids $i --region $REGION --output json`
  status=`echo $details | jq -r '.Reservations[0].Instances[0].State.Name'`

#  echo $details | jq
#  read -p "Press [Enter] key to start backup..."

  name=`echo $details | jq -r '.Reservations[0].Instances[0].Tags | .[] | select(.Key=="Name") | .Value'`
  ip=`echo $details | jq -r '.Reservations[0].Instances[0].NetworkInterfaces[0].PrivateIpAddress'`
  type=`echo $details | jq -r '.Reservations[0].Instances[0].InstanceType'`
  asg=`echo $details | jq '.Reservations[0].Instances[0].Tags | any(.[]; .Key == "Provision")'`
  echo "${bold}$name${normal} - $type ($i)"
  echo "\"$name\",\"$i\",\"$type\",\"$status\",\"$asg\"" >> $csv_name
done