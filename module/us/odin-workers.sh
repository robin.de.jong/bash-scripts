#!/bin/bash

USER=centos
INSTANCES=(
"i-032c56ce8f74d4c12" # Worker 1
"i-00d97587bec09b0d1" # Worker 2
)

START_COMMAND=$(cat << EOF
sudo -u oohlala -- sh -c "
source /home/oohlala/oohlala_env/bin/activate &&
 ./home/oohlala/oohlalamobile/course_response_workerd start 10 || true &&
 ./home/oohlala/oohlalamobile/assignment_response_workerd start 8 || true &&
 ./home/oohlala/oohlalamobile/discussion_response_workerd start 10 || true &&
 ./home/oohlala/oohlalamobile/grade_response_workerd start || true &&
 ./home/oohlala/oohlalamobile/roster_response_workerd start 5 || true &&
 ./home/oohlala/oohlalamobile/announcement_response_workerd start 10 || true &&
 ./home/oohlala/oohlalamobile/quiz_response_workerd start 3 || true
"
EOF)

STOP_COMMAND=$(cat << EOF
sudo -u oohlala -- sh -c "
source /home/oohlala/oohlala_env/bin/activate &&
 ./home/oohlala/oohlalamobile/course_response_workerd stop 10 || true &&
 ./home/oohlala/oohlalamobile/assignment_response_workerd stop 8 || true &&
 ./home/oohlala/oohlalamobile/discussion_response_workerd stop 10 || true &&
 ./home/oohlala/oohlalamobile/grade_response_workerd stop 10 || true &&
 ./home/oohlala/oohlalamobile/roster_response_workerd stop 5 || true &&
 ./home/oohlala/oohlalamobile/announcement_response_workerd stop 10 || true &&
 ./home/oohlala/oohlalamobile/quiz_response_workerd stop 3 || true
"
EOF)

STATUS_COMMAND=$(cat << EOF
sudo -u oohlala -- sh -c "
source /home/oohlala/oohlala_env/bin/activate &&
python /home/oohlala/oohlalamobile/bg_services/jobq_monitor.py
"
EOF)

start () {
  for instance in ${INSTANCES[@]}; do
    echo "[odin-workers:start] Starting on instance: $instance"
    ./scripts/aws-ssh --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "$START_COMMAND"
  done
}

stop() {
  for instance in ${INSTANCES[@]}; do
    echo "[odin-workers:stop] Starting on instance: $instance"
    ./scripts/aws-ssh --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "$STOP_COMMAND"
  done
}

dry-run() {
  for instance in ${INSTANCES[@]}; do
    echo "[odin-workers:dry-run] Checking access on instance: $instance"
    ./scripts/aws-ssh --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "$STATUS_COMMAND"
  done
}


if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi