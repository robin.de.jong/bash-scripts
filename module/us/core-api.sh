#!/bin/bash

SECURITY_GROUP=prod-web-api
USER=centos
instance_ids=(`./scripts/aws-asg-info $SECURITY_GROUP | tr -d \'\"`)

START_COMMAND="sudo systemctl start uwsgi"
STOP_COMMAND="sudo systemctl stop uwsgi"
STATUS_COMMAND="sudo grep \"200)\" /var/run/uwsgi/uwsgi.log"

start() {
  for i in "${instance_ids[@]}"; do
    echo "[core-api:start] Checking from asg: ${bold}$SECURITY_GROUP${normal} instance: ${bold}$i${normal}"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $i "$START_COMMAND"
    echo
  done
}

stop() {
  for i in "${instance_ids[@]}"; do
    echo "[core-api:stop] Checking from asg: ${bold}$SECURITY_GROUP${normal} instance: ${bold}$i${normal}"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $i "$STOP_COMMAND"
    echo
  done
}

dry-run() {
  for i in "${instance_ids[@]}"; do
    echo "[core-api:dry-run] Checking access on asg: ${bold}$SECURITY_GROUP${normal} instance: ${bold}$i${normal}"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $i $STATUS_COMMAND
  done
}

if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi