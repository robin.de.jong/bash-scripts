#!/bin/bash

USER=centos
INSTANCE=i-0cf1a405c98f9fbb0

SERVICES=("dramatiq-default" "dramatiq-feed-sync" "dramatiq-push-v3" "dramatiq-push" "dramatiq-sequence")

start() {
  echo "[core-workers:stop] Starting on instance: $INSTANCE"
  for service in ${SERVICES[@]}; do
    echo "[core-workers:start] Starting service: $service"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "sudo systemctl start $service"
  done
}

stop() {
  echo "[core-workers:stop] Stopping on instance: $INSTANCE"
  for service in ${SERVICES[@]}; do
    echo "[core-workers:stop] Stopping service: $service"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "sudo systemctl stop $service"
  done
}

dry-run() {
  echo "[core-workers:dry-run] Checking access on instance: $INSTANCE"
  for service in ${SERVICES[@]}; do
    echo "[core-workers:dry-run] Status of service: $service"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "sudo systemctl status $service"
  done
}

if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi