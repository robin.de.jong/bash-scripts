#!/bin/bash

USER=centos
INSTANCE=i-0194b90874e957e48

START_COMMAND="sudo systemctl start mode-bridge"
STOP_COMMAND="sudo systemctl stop mode-bridge"
STATUS_COMMAND="sudo systemctl status mode-bridge"

start () {
  echo "[mode-bridge:start] Starting on instance $INSTANCE"
  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER $INSTANCE "$START_COMMAND"
}

stop() {
  echo "[mode-bridge:stop] Stopping on instance $INSTANCE"
  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER $INSTANCE "$STOP_COMMAND"
}

dry-run() {
  echo "[mode-bridge:dry-run] Status on instance $INSTANCE"
  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER $INSTANCE "$STATUS_COMMAND"
}

if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi