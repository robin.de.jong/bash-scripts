#!/bin/bash

USER=centos

OFFLINE_INSTANCE=i-0ee76bf8845904848
#MAINTENANCE_INSTANCES=("i-016b832764aa68f49" "i-03bd7f2b870a3e89d") # Last entry has all jobs commented out
MAINTENANCE_INSTANCES=("i-016b832764aa68f49")

# Prepend the whole crontab file with a '#'.
BACKUP_CRON=$(cat << EOF
yes | sudo rm -f /root/crontab-root-backup;
yes | sudo cp /var/spool/cron/root /root/crontab-root-backup;
sudo sed -i 's/\(.*\)/#\1/' /var/spool/cron/root
EOF)

# Restore the backed-up file back
RESTORE_CRON=$(cat << EOF
sudo mv /root/crontab-root-backup /var/spool/cron/root;
EOF)

start() {
#  echo "[offline-maintenance:start] Starting on instance: $OFFLINE_INSTANCE"
#  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $OFFLINE_INSTANCE "$RESTORE_CRON"
  for instance in ${MAINTENANCE_INSTANCES[@]}; do
    echo "[offline-maintenance:start] Starting on instance: $instance"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "$RESTORE_CRON"
  done

  # After stopping the services, we have to wait until all jobs have stopped running
#  wait_cronjob_stopped "[o]ohlala-offline-scripts" $OFFLINE_INSTANCE
  for instance in ${MAINTENANCE_INSTANCES[@]}; do
    wait_cronjob_stopped "[o]ohlala-maintenance" $instance
  done
}

stop() {
#  echo "[offline-maintenance:stop] Stopping on instance: $OFFLINE_INSTANCE"
#  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $OFFLINE_INSTANCE "$BACKUP_CRON"
  for instance in ${MAINTENANCE_INSTANCES[@]}; do
    echo "[offline-maintenance:stop] Stopping on instance: $instance"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "$BACKUP_CRON"
  done

  # After stopping the services, we have to wait until all jobs have stopped running
  wait_cronjob_stopped "[o]ohlala-offline-scripts" $OFFLINE_INSTANCE
  for instance in ${MAINTENANCE_INSTANCES[@]}; do
    wait_cronjob_stopped "[o]ohlala-maintenance" $instance
  done
}

wait_cronjob_stopped() {
  instance=$2
  process=$1

  while true; do
      ps=`./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep $process"`
      if [ -z "$ps" ]; then
        echo "[offline-maintenance:stop] Done waiting, on instance with id $instance"
        break
      fi

      echo "[offline-maintenance:stop] Waiting for cronjobs to stop, on instance with id $instance"
      echo $ps
      sleep 10
  done
}

wait_cronjob_started() {
  instance=$2
  process=$1

  while true; do
      ps=`./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep $process"`
      if [ "$ps" ]; then
        echo "[offline-maintenance:start] Done waiting, on instance with id $instance"
        break
      fi

      echo "[offline-maintenance:start] Waiting for cronjobs to start, on instance with id $instance"
      echo $ps
      sleep 10
  done
}

dry-run() {
#  echo "[offline-maintenance:dry-run] Checking access on instance: $OFFLINE_INSTANCE"
#  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $OFFLINE_INSTANCE "ps aux | grep [o]ohlala-offline-scripts"
  for instance in ${MAINTENANCE_INSTANCES[@]}; do
    echo "[offline-maintenance:dry-run] Checking access on instance: $instance"
    ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep [o]ohlala-maintenance"
  done
}


if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi