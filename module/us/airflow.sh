#!/bin/bash

USER=centos
INSTANCE=i-0288bb16a6be8f009

SERVICES=("airflow-webserver" "airflow-scheduler")

start () {
  echo "[airflow:start] Starting airflow"
  for service in ${SERVICES[@]}; do
    ./scripts/aws-ssh --bu $BASTION_USER --bk $BASTION_KEY -u $USER $INSTANCE "sudo systemctl start $service"
  done
}

stop() {
  echo "[airflow:stop] Stopping airflow"
  for service in ${SERVICES[@]}; do
    ./scripts/aws-ssh --bu $BASTION_USER --bk $BASTION_KEY -u $USER $INSTANCE "sudo systemctl stop $service"
  done
}

dry-run() {
  echo "[airflow:dry-run] Checking access on instance: $INSTANCE"
  for service in ${SERVICES[@]}; do
    ./scripts/aws-ssh --bu $BASTION_USER --bk $BASTION_KEY -u $USER $INSTANCE "sudo systemctl status $service"
  done
}

if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi