#!/bin/bash


USER=centos
INSTANCE=i-0419482d0dc292ed9

START_COMMAND="sudo service uwsgi start"
STOP_COMMAND="sudo service uwsgi stop"
STATUS_COMMAND="sudo service uwsgi status"

start () {
  echo "[site-main:start] Starting on instance: $INSTANCE"
  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "$START_COMMAND"
  echo "Waiting for site-main to start..."
  wait_uwsgi_started "[u]wsgi.ini" $INSTANCE
}

stop() {
  echo "[site-main:stop] Stopping on instance: $INSTANCE"
  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "$STOP_COMMAND"
  echo "Waiting for site-main to stop..."
  wait_uwsgi_stopped "[u]wsgi.ini" $INSTANCE
}

wait_uwsgi_stopped() {
  instance=$2
  process=$1

  while true; do
      ps=`./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep $process"`
      if [ -z "$ps" ]; then
        echo "[site-main:stop] Done waiting, on instance with id $instance"
        break
      fi

      echo "[site-main:stop] Waiting for uwsgi tasks to stop, on instance with id $instance"
      echo $ps
      sleep 10
  done
}

wait_uwsgi_started() {
  instance=$2
  process=$1

  while true; do
      ps=`./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep $process"`
      if [ "$ps" ]; then
        echo "[site-main:start] Done waiting, on instance with id $instance"
        break
      fi

      echo "[site-main:start] Waiting for uwsgi to start, on instance with id $instance"
      echo $ps
      sleep 10
  done
}

dry-run() {
  echo "[site-main:dry-run] Checking access on instance: $INSTANCE"
  ./scripts/aws-ssh --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "$STATUS_COMMAND"
}


if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi