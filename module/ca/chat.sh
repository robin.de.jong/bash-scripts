#!/bin/bash

USER=centos
INSTANCE=i-06b106cbf8af1df5f # Production xmpp

# On staging it's located on /opt/ejabberd-17.08/bin/ejabberdctl
START_COMMAND="sudo -u ejabberd /opt/ejabberd-17.09/bin/ejabberdctl start"
STOP_COMMAND="sudo -u ejabberd /opt/ejabberd-17.09/bin/ejabberdctl stop"
STATUS_COMMAND="sudo -u ejabberd /opt/ejabberd-17.09/bin/ejabberdctl status"

start () {
  echo "[chat:start] Starting chat on instance $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER $INSTANCE "$START_COMMAND"
}

stop() {
  echo "[chat:stop] Stopping chat on instance $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER $INSTANCE "$STOP_COMMAND"
}

dry-run() {
  echo "[chat:dry-run] Checking access on instance: $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu $BASTION_USER --bk $BASTION_KEY -u $USER $INSTANCE "$STATUS_COMMAND"
}

if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi