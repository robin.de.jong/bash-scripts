#!/bin/bash

USER=centos
INSTANCE=i-0e8ed6b91db424bc1

# Prepend the whole crontab file with a '#'.
BACKUP_CRON=$(cat << EOF
yes | sudo rm -f /root/crontab-root-backup;
yes | sudo cp /var/spool/cron/root /root/crontab-root-backup;
sudo sed -i 's/\(.*\)/#\1/' /var/spool/cron/root
EOF)

# Restore the backed-up file back
RESTORE_CRON=$(cat << EOF
sudo mv /root/crontab-root-backup /var/spool/cron/root;
EOF)

start() {
  echo "[es-indexer:start] Starting on instance: $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "$RESTORE_CRON"

  # After stopping the services, we have to wait until all jobs have stopped running
  wait_cronjob_stopped "[i]ndex" $OFFLINE_INSTANCE
}

stop() {
  echo "[es-indexer:start] Stopping on instance: $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "$BACKUP_CRON"

  # After stopping the services, we have to wait until all jobs have stopped running
  wait_cronjob_stopped "[i]ndex" $OFFLINE_INSTANCE
}

wait_cronjob_stopped() {
  instance=$2
  process=$1

  while true; do
      ps=`./scripts/aws-ssh -r ca-central-1 ---public -bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep $process"`
      if [ -z "$ps" ]; then
        echo "[es-indexer:stop] Done waiting, on instance with id $instance"
        break
      fi

      echo "[es-indexer:stop] Waiting for cronjobs to stop, on instance with id $instance"
      echo $ps
      sleep 10
  done
}

wait_cronjob_started() {
  instance=$2
  process=$1

  while true; do
      ps=`./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep $process"`
      if [ "$ps" ]; then
        echo "[es-indexer:start] Done waiting, on instance with id $instance"
        break
      fi

      echo "[es-indexer:start] Waiting for cronjobs to start, on instance with id $instance"
      echo $ps
      sleep 10
  done
}

dry-run() {
  echo "[es-indexer:dry-run] Checking access on instance: $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu $BASTION_USER --bk $BASTION_KEY -u $USER $INSTANCE "ps aux | grep [i]ndex"
}


if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi