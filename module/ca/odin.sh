#!/bin/bash


USER=centos
INSTANCE=i-06866c9e41461e399

START_COMMAND="sudo /home/oohlala/oohlala_env_35/bin/uwsgi --ini /etc/uwsgi.ini --daemonize /var/log/uwsgi.log"
STOP_COMMAND="sudo pkill uwsgi"

start () {
  echo "[odin:start] Starting odin on instance: $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "$START_COMMAND"
  echo "Waiting for odin to start..."
  wait_uwsgi_started "[u]wsgi.ini" $INSTANCE
}

stop() {
  echo "[odin:stop] Stopping odin on instance: $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $INSTANCE "$STOP_COMMAND"
  echo "Waiting for odin to stop..."
  wait_uwsgi_stopped "[u]wsgi.ini" $INSTANCE
}

wait_uwsgi_stopped() {
  instance=$2
  process=$1

  while true; do
      ps=`./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep $process"`
      if [ -z "$ps" ]; then
        echo "[odin:stop] Done waiting, on instance with id $instance"
        break
      fi

      echo "[odin:stop] Waiting for uwsgi tasks to stop, on instance with id $instance"
      echo $ps
      sleep 10
  done
}

wait_uwsgi_started() {
  instance=$2
  process=$1

  while true; do
      ps=`./scripts/aws-ssh -r ca-central-1 --public --bu=$BASTION_USER --bk=$BASTION_KEY -u $USER -n $instance "ps aux | grep $process"`
      if [ "$ps" ]; then
        echo "[odin:start] Done waiting, on instance with id $instance"
        break
      fi

      echo "[odin:start] Waiting for uwsgi to start, on instance with id $instance"
      echo $ps
      sleep 10
  done
}

dry-run() {
  echo "[odin:dry-run] Checking access on instance: $INSTANCE"
  ./scripts/aws-ssh -r ca-central-1 --public --bu $BASTION_USER --bk $BASTION_KEY -u $USER $INSTANCE "ps aux | grep [u]wsgi"
}


if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi