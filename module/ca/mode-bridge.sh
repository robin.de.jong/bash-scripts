#!/bin/bash

start () {
  echo "[mode-bridge:start] Nothing here"
}

stop() {
  echo "[mode-bridge:stop] Nothing here"
}

dry-run() {
  echo "[mode-bridge:dry-run] Nothing here"
}

if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi