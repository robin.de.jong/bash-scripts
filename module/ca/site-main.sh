#!/bin/bash

START_COMMAND="sudo /home/oohlala/oohlalamobile/start-uwsgi.sh"
STOP_COMMAND="sudo /home/oohlala/oohlalamobile/stop-uwsgi.sh"
STATUS_COMMAND="ps aux | grep [u]wsgi"

start () {
  echo "[site-main:start] Starting"
  ssh -i ~/.ssh/legacy_canada_key -o 'HostKeyAlgorithms +ssh-rsa' -o 'PubkeyAcceptedAlgorithms +ssh-rsa' -p 2209 oohadmin@209.44.99.139 "$START_COMMAND"
}

stop() {
  echo "[site-main:stop] Stopping"
  ssh -i ~/.ssh/legacy_canada_key -o 'HostKeyAlgorithms +ssh-rsa' -o 'PubkeyAcceptedAlgorithms +ssh-rsa' -p 2209 oohadmin@209.44.99.139 "$STOP_COMMAND"
}

dry-run() {
  ssh -i ~/.ssh/legacy_canada_key -o 'HostKeyAlgorithms +ssh-rsa' -o 'PubkeyAcceptedAlgorithms +ssh-rsa' -p 2209 oohadmin@209.44.99.139 "$STATUS_COMMAND"
}

if [[ $1 == "start" ]]; then
  start
elif [[ $1 == "stop" ]]; then
  stop
elif [[ $1 == "dry-run" ]]; then
  dry-run
fi