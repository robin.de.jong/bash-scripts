#!/bin/bash

bold=$(tput bold)
underscore=$(tput smul)
normal=$(tput sgr0)

BASTION_USER=robin_de_jong
BASTION_KEY=~/.ssh/id_ed25519

LC_ALL=C
SECURITY_GROUP=prod-web-api
region=us-east-1
USER=centos
instance_ids=($(./scripts/aws-asg-info $SECURITY_GROUP $region | tr -d \'\"))
template_server="i-0b738bc92a5d05614"

IAM_ROLE=$1

function apply_iam() {
  instance=$1
  iam=$2
  echo "${bold}Applying on $iam onto $instance${normal}"
#  aws ec2 associate-iam-instance-profile --instance-id $instance --iam-instance-profile Arn=$iam --region=$region
#  aws-ssh $instance -r $region "sudo systemctl restart amazon-cloudwatch-agent"
  aws-ssh $instance -r $region "sudo ls -l /var/log/oohlala/api_request.log"
  aws-ssh $instance -r $region "sudo tail /var/log/oohlala/api_request.log"
  aws-ssh $instance -r $region "sudo tail /var/log/amazon/amazon-cloudwatch-agent/amazon-cloudwatch-agent.log"
}

#apply_iam $template_server $IAM_ROLE
for i in "${instance_ids[@]}"; do
  apply_iam $i $IAM_ROLE
done

#arn:aws:iam::511871420126:instance-profile/prod_can_web_api
#arn:aws:iam::511871420126:instance-profile/prod_web_api
