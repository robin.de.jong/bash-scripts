# RDS Details

## US

```bash
> ./rds-details us-production-log-cluster
------------------------------- General ------------------------------
Cluster: 		us-production-log-cluster
---------------------------- Connectivity ----------------------------
VPC: 			oohlala-use-production (vpc-e39ee99a)
DB Subnet Group: 	us-production-log-db
VPC Security Group: 	vpc-mysql-db-only (sg-d2505ca3)
> Additional configuration
Database Port: 		3306
---------------------- Additional configuration ----------------------
DB cluster parameter group: oohlala-prod-aurora56
> Backup
Copy tags to snapshots: false
-- Backtrack --
Enable backtrack: false
> Encryption
Enable encryption: false
> Log exports
Audit log: false
Error log: false
General log: false
Slow query log: false
> Maintenance
Enable auto minor version upgrade: false
> Delete protection
Enable delete protection: false
```

```bash
> ./rds-details us-production-main-cluster
------------------------------- General ------------------------------
Cluster: 		us-production-main-cluster
---------------------------- Connectivity ----------------------------
VPC: 			oohlala-use-production (vpc-e39ee99a)
DB Subnet Group: 	us-production-log-db
VPC Security Group: 	vpc-mysql-main-db-only (sg-0b31277e)
> Additional configuration
Database Port: 		3306
---------------------- Additional configuration ----------------------
DB cluster parameter group: oohlala-prod-aurora56
> Backup
Copy tags to snapshots: false
-- Backtrack --
Enable backtrack: false
> Encryption
Enable encryption: false
> Log exports
Audit log: false
Error log: true
General log: false
Slow query log: true
> Maintenance
Enable auto minor version upgrade: false
> Delete protection
Enable delete protection: false
```

```bash
> ./rds-details us-production-chat-cluster
------------------------------- General ------------------------------
Cluster: 		us-production-chat-cluster
---------------------------- Connectivity ----------------------------
VPC: 			oohlala-use-production (vpc-e39ee99a)
DB Subnet Group: 	us-production-log-db
VPC Security Group: 	vpc-mysql-chat-db-only (sg-c74a96b2)
> Additional configuration
Database Port: 		3306
---------------------- Additional configuration ----------------------
DB cluster parameter group: oohlala-prod-aurora56
> Backup
Copy tags to snapshots: false
-- Backtrack --
Enable backtrack: false
> Encryption
Enable encryption: false
> Log exports
Audit log: false
Error log: false
General log: false
Slow query log: false
> Maintenance
Enable auto minor version upgrade: false
> Delete protection
Enable delete protection: false
```

## Canada

```bash
> ./rds-details can-production-main-cluster ca-central-1
------------------------------- General ------------------------------
Cluster: 		can-production-main-cluster
---------------------------- Connectivity ----------------------------
VPC: 			oohlala-can (vpc-a49f5dcd)
DB Subnet Group: 	can-production-chat-db
VPC Security Group: 	vpc-mysql-main-db-only (sg-752da91d)
> Additional configuration
Database Port: 		3306
---------------------- Additional configuration ----------------------
DB cluster parameter group: oohlala-prod-aurora56
> Backup
Copy tags to snapshots: false
-- Backtrack --
Enable backtrack: false
> Encryption
Enable encryption: false
> Log exports
Audit log: false
Error log: false
General log: false
Slow query log: false
> Maintenance
Enable auto minor version upgrade: false
> Delete protection
Enable delete protection: false
```

```bash
> ./rds-details can-production-chat-cluster ca-central-1
------------------------------- General ------------------------------
Cluster: 		can-production-chat-cluster
---------------------------- Connectivity ----------------------------
VPC: 			oohlala-can (vpc-a49f5dcd)
DB Subnet Group: 	can-production-chat-db
VPC Security Group: 	rds-launch-wizard (sg-5360a93b)
> Additional configuration
Database Port: 		3306
---------------------- Additional configuration ----------------------
DB cluster parameter group: oohlala-prod-aurora56
> Backup
Copy tags to snapshots: false
-- Backtrack --
Enable backtrack: false
> Encryption
Enable encryption: false
> Log exports
Audit log: false
Error log: false
General log: false
Slow query log: false
> Maintenance
Enable auto minor version upgrade: false
> Delete protection
Enable delete protection: false
```

## Extra

```bash
> ./rds-details us-external-data-export
------------------------------- General ------------------------------
Cluster: 		us-external-data-export
---------------------------- Connectivity ----------------------------
VPC: 			oohlala-use-production (vpc-e39ee99a)
DB Subnet Group: 	us-production-log-db
VPC Security Group: 	vpc-mysql-external-data-only (sg-0e7512bc8e54c7ef4)
> Additional configuration
Database Port: 		3306
---------------------- Additional configuration ----------------------
DB cluster parameter group: oohlala-prod-aurora56
> Backup
Copy tags to snapshots: true
-- Backtrack --
Enable backtrack: false
> Encryption
Enable encryption: true
> Log exports
Audit log: false
Error log: false
General log: false
Slow query log: false
> Maintenance
Enable auto minor version upgrade: false
> Delete protection
Enable delete protection: true
```